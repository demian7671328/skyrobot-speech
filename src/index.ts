import OpenAI from "openai";
import dotenv from "dotenv";
import fs from "fs";
import path from "path";
import soundPlay from "sound-play";
import { exec } from "child_process";
import ElevenLabs from "elevenlabs-node";
import { Pinecone } from "@pinecone-database/pinecone";

dotenv.config();

const openai = new OpenAI({
  apiKey: process.env.OPENAI_API_KEY,
});

const voice = new ElevenLabs({
  apiKey: `${process.env.ELEVENLABS_API_KEY}`, // Your API key from Elevenlabs
  voiceId: "rlVSkz1fjmsHRRpHMUkU", // A Voice ID from Elevenlabs
});

const pc = new Pinecone({
  apiKey: process.env.PINECONE_API_KEY!,
});

const config = {
  similarityQuery: {
    topK: 4,
    includeMetadata: true,
    includeValues: false,
  },
  namespace: "jarvis-skyro",
  indexName: "jarvis-skyro",
  embeddingId: "jarvis-skyro-embedding",
  dimensions: 3072,
  metrics: "cosine",
  cloud: "aws",
  region: "us-west-2",
  query: "Čo je to Skyro?",
};

async function queryEmbeddings(queryText: string) {
  const queryEmbedding = await openai.embeddings.create({
    model: "text-embedding-3-large",
    input: queryText,
  });

  const result = await pc
    .index(config.indexName)
    .namespace(config.namespace)
    .query({
      ...config.similarityQuery,
      vector: queryEmbedding.data[0].embedding,
    });

  return result.matches;
}

let isRecording = false; // Flag to track the recording state
let recordingProcess = null as any; // Store the recording process

function startRecording() {
  console.log("Listening...");

  // Define the audio file name and path
  const audioPath = path.resolve("./audio.wav");

  // Delete the existing audio file if it exists
  if (fs.existsSync(audioPath)) {
    fs.unlinkSync(audioPath);
    console.log("Existing audio file deleted.");
  }

  // Start recording audio from the default microphone using ffmpeg
  recordingProcess = exec(
    `ffmpeg -f avfoundation -i ":0" -ac 2 -ar 44100 ${audioPath}`
  );

  // Handle process stdout (you can log or discard it)
  recordingProcess.stdout?.on("data", (data: any) => {
    console.log(data.toString());
  });

  // Handle process stderr (log errors if any)
  recordingProcess.stderr?.on("data", (data: any) => {
    console.error(data.toString());
  });

  isRecording = true;
}

function stopRecording() {
  console.log("Stopping recording...");

  // Stop the recording
  if (recordingProcess) {
    recordingProcess.kill("SIGINT"); // Sends the interrupt signal to ffmpeg
    recordingProcess = null;
    isRecording = false;
  }
}

async function transcribeAudio(filename: string) {
  const audio = fs.createReadStream(filename);

  // Transcribe the audio file using the OpenAI API
  const response = await openai.audio.transcriptions.create({
    model: "whisper-1",
    file: audio,
    language: "sk",
  });

  return response.text;
}

async function getOpenAIResponse(transcript: string) {
  const matches = await queryEmbeddings(transcript);
  const response = await openai.chat.completions.create({
    model: "gpt-4-turbo-preview",
    messages: [
      {
        role: "system",
        content: `You are a chatbot named SkyroBot. You are here to help students with their questions about new real high-school Skyro that will open in 2024. Skyro is real high-school.
        Please remember to write very briefly and clearly. Max 2-3 sentences.
              Based on the message from user you will be provided with a context reference to the most similar embeddings in the database. You can use this context to provide more accurate and relevant answers.\n\n
    
              Contexts: \n${matches
                .map((match) => match.metadata?.text)
                .join("\n\n")}
              `,
      },
      {
        role: "user",
        content: transcript,
      },
    ],
    temperature: 0.6,
  });

  return response.choices[0].message.content;
}

const speechFile = path.resolve("./speech.mp3");

async function convertResponseToAudioAndPlay(response: string) {
  //   const mp3 = await openai.audio.speech.create({
  //     model: "tts-1",
  //     input: response,
  //     voice: "echo",
  //   });

  //   const buffer = Buffer.from(await mp3.arrayBuffer());
  //   await fs.promises.writeFile(speechFile, buffer);

  await voice.textToSpeech({
    fileName: "speech.mp3",
    textInput: response,
    voiceId: "1RJRrinw5Sl6L4PzYdcp",
    modelId: "eleven_multilingual_v2",
    speakerBoost: true,
  });

  // Play the audio file
  await soundPlay.play(speechFile);
}

async function main() {
  console.log("Press enter to start/stop recording.");
  process.stdin.on("data", async () => {
    if (!isRecording) {
      startRecording();
    } else {
      stopRecording();
      const transcript = await transcribeAudio("audio.wav");
      console.log("Transcript:", transcript);

      const response = await getOpenAIResponse(transcript);
      console.log("Assistant Response:", response);

      await convertResponseToAudioAndPlay(response!);
    }
  });
}

main().catch(console.error);
